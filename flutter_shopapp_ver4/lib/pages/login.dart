import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopapp/components/app_data.dart';
import 'package:flutter_shopapp/components/app_methods.dart';
import 'package:flutter_shopapp/components/app_tools.dart';
import 'package:flutter_shopapp/components/firebase_methods.dart';
import 'package:flutter_shopapp/pages/home.dart';
import 'signup.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController email = new TextEditingController();
  TextEditingController password = new TextEditingController();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  BuildContext context;
  AppMethods appMethods = new FirebaseMethods();

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return new Scaffold(
      key: scaffoldKey,
      backgroundColor: Theme.of(context).primaryColor,
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
              image: AssetImage("images/login-bg.jpg"),
              fit: BoxFit.cover,
            )),
          ),
          
          Container(
            width: double.infinity,
              height: double.infinity,
            color: Colors.black.withOpacity(0.7),
          ),
          
          
          SingleChildScrollView(
            child: new Column(
              children: <Widget>[
                new SizedBox(
                  height: 100.0,
                ),
                AvatarGlow(
                  glowColor: Colors.white,
                  endRadius: 90.0,
                  duration: Duration(milliseconds: 2000),
                  repeat: true,
                  showTwoGlows: true,
                  repeatPauseDuration: Duration(milliseconds: 100),
                  child: Material(
                    elevation: 8.0,
                    shape: CircleBorder(),
                    child: CircleAvatar(
                      backgroundColor: Colors.grey[100],
                      child: CircleAvatar(
                        backgroundImage: AssetImage('images/logo.png'),
                        radius: 60.0,
                      ),
                      radius: 62.0,
                    ),
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      appTextField(
                        isPassword: false,
                        textHint: "Tên đăng nhập",
                        textIcon: Icons.email,
                        controller: email,
                      ),
                      new SizedBox(
                        height: 40.0,
                      ),
                      appTextField(
                          isPassword: true,
                          textHint: "Mật khẩu",
                          textIcon: Icons.lock,
                          controller: password),

                      Padding(
                        padding:
                        const EdgeInsets.only(left: 50.0, right: 50.0, top: 30.0),
                        child: Divider(
                          color: Colors.white,
                        ),
                      ),

                      appButton(
                        btnTxt: "Đăng nhập",
                        onBtnclicked: verifyLoggin,
                        btnPadding: 20,
                        btnColor: Theme.of(context).primaryColor,
                      ),


                      new GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (context) => new SingUp()));
                        },
                        child: new Text(
                          "Bạn chưa có tài khoản? Đăng ký tại đây",
                          style: new TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  verifyLoggin() async {
    if (email.text == "") {
      showSnackbar("Vui lòng không để trống Email !", scaffoldKey);
      return;
    }

    if (password.text == "") {
      showSnackbar("Vui lòng không để trống mật khẩu !", scaffoldKey);
      return;
    }

    displayProgressDialog(context);
    String response =
        await appMethods.loginUser(email: email.text, password: password.text);
    if (response == successful) {
      closeProgressDialog(context);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => new HomePage()));
    } else {
      closeProgressDialog(context);
      showSnackbar(response, scaffoldKey);
    }
  }
}

//Old case
//void submit() {
//  final form = formKey.currentState;
//  if (form.validate()) {
//    form.save();
//
//    performLogin();
//  }
//}
//
//void performLogin() {
//  final snackbar = new SnackBar(content: new Text("Errordfdsfsd!"));
//  scaffoldKey.currentState.showSnackBar(snackbar);
//}

//class Login extends StatefulWidget {
//  @override
//  _LoginState createState() => _LoginState();
//}
//
//class _LoginState extends State<Login> {
//  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
//  final _formKey = GlobalKey<FormState>();
//  TextEditingController _emailTextController = TextEditingController();
//  TextEditingController _passwordTextController = TextEditingController();
//
//  SharedPreferences preferences;
//  bool loading = false;
//  bool isLogedin = false;
//
//  @override
//  void initState() {
//    super.initState();
//    isSignedIn();
//  }
//
//  void isSignedIn() async {
//    setState(() {
//      loading = true;
//    });
//
//    if (isLogedin) {
//      Navigator.pushReplacement(
//          context,
//          MaterialPageRoute(
//              builder: (context) =>
//                  HomePage())); //pushReplacement: không cho user có khả năng back lại trang login trừ khi logout
//    }
//
//    setState(() {
//      loading = false;
//    });
//  }
//
////  Future handleSignIn() async {
////    preferences = await SharedPreferences.getInstance();
////
////    setState(() {
////      loading = true;
////    });
//
//
//
//  @override
//  Widget build(BuildContext context) {
//    double height = MediaQuery.of(context).size.height / 3;
//    return Scaffold(
//      body: Stack(
//        children: <Widget>[
//          Container(
//            decoration: BoxDecoration(
//                image: new DecorationImage(
//              image: new ExactAssetImage('images/login-bg.jpg'),
//              fit: BoxFit.cover,
//            )),
//          ),
//////TODO:: make the logo show?
//          Container(
//            color: Colors.black.withOpacity(0.7),
//            width: double.infinity,
//            height: double.infinity,
//          ),
//
//          Padding(
//            padding: const EdgeInsets.only(top: 40.0),
//            child: Center(
//              child: Form(
//                key: _formKey,
//                child: Column(
//                  children: <Widget>[
//                    Container(
//                      padding: const EdgeInsets.only(top: 5.0),
//                      child: Column(
//                        mainAxisAlignment: MainAxisAlignment.center,
//                        children: <Widget>[
//                          AvatarGlow(
//                            glowColor: Colors.white,
//                            endRadius: 90.0,
//                            duration: Duration(milliseconds: 2000),
//                            repeat: true,
//                            showTwoGlows: true,
//                            repeatPauseDuration: Duration(milliseconds: 100),
//                            child: Material(
//                              elevation: 8.0,
//                              shape: CircleBorder(),
//                              child: CircleAvatar(
//                                backgroundColor: Colors.grey[100],
//                                child: CircleAvatar(
//                                  backgroundImage:
//                                      AssetImage('images/logo.png'),
//                                  radius: 60.0,
//                                ),
//                                radius: 62.0,
//                              ),
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
//                      child: Material(
//                        borderRadius: BorderRadius.circular(10.0),
//                        color: Colors.white.withOpacity(0.8),
//                        elevation: 0.0,
//                        child: Padding(
//                          padding: const EdgeInsets.only(left: 12.0),
//                          child: TextFormField(
//                            controller: _emailTextController,
//                            decoration: InputDecoration(
//                              hintText: "Email",
//                              icon: Icon(Icons.email),
//                            ),
//                            validator: (value) {
//                              if (value.isEmpty) {
//                                Pattern pattern =
//                                    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
//                                RegExp regex = new RegExp(pattern);
//                                if (!regex.hasMatch(value))
//                                  return "Đia chỉ Email không hợp lệ!";
//                                else
//                                  return null;
//                              }
//                              return null;
//                            },
//                          ),
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
//                      child: Material(
//                        borderRadius: BorderRadius.circular(10.0),
//                        color: Colors.white.withOpacity(0.8),
//                        elevation: 0.0,
//                        child: Padding(
//                          padding: const EdgeInsets.only(left: 12.0),
//                          child: TextFormField(
//                            controller: _passwordTextController,
//                            decoration: InputDecoration(
//                              hintText: "Mật khẩu",
//                              icon: Icon(Icons.lock_outline),
//                            ),
//                            validator: (value) {
//                              if (value.isEmpty) {
//                                return "Vui lòng không để trống mật khẩu!";
//                              } else if (value.length < 6) {
//                                return "Mật khẩu phải từ 6 ký tự trở lên";
//                              }
//                              return null;
//                            },
//                          ),
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
//                      child: Material(
//                        borderRadius: BorderRadius.circular(20.0),
//                        color: Colors.red.withOpacity(0.8),
//                        elevation: 0.0,
//                        child: MaterialButton(
//                          onPressed: () {},
//                          minWidth: MediaQuery.of(context).size.width,
//                          child: Text(
//                            "Đăng nhập",
//                            textAlign: TextAlign.center,
//                            style: TextStyle(
//                                color: Colors.white,
//                                fontWeight: FontWeight.bold,
//                                fontSize: 22.0),
//                          ),
//                        ),
//                      ),
//                    ),
//                    Padding(
//                        padding: const EdgeInsets.all(8.0),
//                        child: Text(
//                          "Quên mật khẩu?",
//                          textAlign: TextAlign.center,
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontWeight: FontWeight.w400,
//                              fontSize: 13.0),
//                        )),
//                    Padding(
//                      padding: const EdgeInsets.all(8.0),
//                      child: InkWell(
//                        onTap: (){
//                          Navigator.push(context, MaterialPageRoute(builder: (context)=>SignUp()));
//                        },
//                        child: Text("Đăng ký!", style: TextStyle(color: Colors.lightBlue)),
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//            ),
//          ),
//
//          Visibility(
//            visible: loading ?? true,
//            child: Center(
//              child: Container(
//                alignment: Alignment.center,
//                color: Colors.white.withOpacity(0.9),
//                child: CircularProgressIndicator(
//                  valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
//                ),
//              ),
//            ),
//          ),
//        ],
//      ),
//    );
//  }
//}

//validator: (value){
//if(value.isEmpty){
//Pattern pattern =
//r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
//RegExp regex = new RegExp(pattern);
//if (!regex.hasMatch(value))
//return 'Đia chỉ Email không hợp lệ!';
//else
//return null;
//}
//},
