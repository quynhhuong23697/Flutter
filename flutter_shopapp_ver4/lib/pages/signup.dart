import 'package:flutter/material.dart';
import 'package:flutter_shopapp/components/app_data.dart';
import 'package:flutter_shopapp/components/app_methods.dart';
import 'package:flutter_shopapp/components/app_tools.dart';
import 'package:flutter_shopapp/components/firebase_methods.dart';
import 'package:flutter_shopapp/pages/login.dart';

class SingUp extends StatefulWidget {
  @override
  _SingUpState createState() => _SingUpState();
}

class _SingUpState extends State<SingUp> {
  TextEditingController fullname = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController re_passwprd = new TextEditingController();
  TextEditingController location = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController phone = new TextEditingController();

  String gender;
  String groupValue = "Nam";

  final scaffoldKey = new GlobalKey<ScaffoldState>();
  BuildContext context;
  AppMethods appMethods = new FirebaseMethods();

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return new Scaffold(
      key: scaffoldKey,
      backgroundColor: Theme
          .of(context)
          .primaryColor,
      body: Stack(
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("images/login-bg.jpg"),
                    fit: BoxFit.cover,
                  ))),

          Container(
            width: double.infinity,
            height: double.infinity,
            color: Colors.black.withOpacity(0.7),
          ),


          SingleChildScrollView(
            child: new Column(
              children: <Widget>[


                new SizedBox(
                  height: 70.0,
                ),
                appTextField(
                  isPassword: false,
                  textHint: "Họ và tên",
                  textIcon: Icons.person,
                  controller: fullname,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          child: ListTile(
                            title: Text("Nam",
                                textAlign: TextAlign.end,
                                style: TextStyle(color: Colors.white)),
                            trailing: Radio(
                              activeColor: Colors.white,
                              value: "Nam",
                              groupValue: groupValue,
                              onChanged: (e) => valueChange(e),
                            ),
                          )),
                      Expanded(
                          child: ListTile(
                            title: Text("Nữ",
                                textAlign: TextAlign.end,
                                style: TextStyle(color: Colors.white)),
                            trailing: Radio(
                              activeColor: Colors.white,
                              value: "Nữ",
                              groupValue: groupValue,
                              onChanged: (e) => valueChange(e),
                            ),
                          )),
                    ],
                  ),
                ),
                appTextField(
                  isPassword: false,
                  textHint: "Email",
                  textIcon: Icons.email,
                  controller: email,
                  textType: TextInputType.emailAddress,
                ),
                new SizedBox(
                  height: 25.0,
                ),
                appTextField(
                    isPassword: true,
                    textHint: "Mật khẩu",
                    textIcon: Icons.lock,
                    controller: password),
                new SizedBox(
                  height: 25.0,
                ),
                appTextField(
                    isPassword: true,
                    textHint: "Nhập lại mật khẩu",
                    textIcon: Icons.lock,
                    controller: re_passwprd),
                new SizedBox(
                  height: 25.0,
                ),
                appTextField(
                  isPassword: false,
                  textHint: "Địa chỉ",
                  textIcon: Icons.location_on,
                  controller: location,
                ),
                new SizedBox(
                  height: 25.0,
                ),
                appTextField(
                    isPassword: false,
                    textHint: "Điện thoại",
                    textIcon: Icons.phone,
                    controller: phone,
                    textType: TextInputType.number),
                Padding(
                  padding:
                  const EdgeInsets.only(left: 50.0, right: 50.0, top: 30.0),
                  child: Divider(
                    color: Colors.white,
                  ),
                ),
                appButton(
                  btnTxt: "Đăng ký",
                  onBtnclicked: verifyLoggin,
                  btnPadding: 20.0,
                  btnColor: Theme
                      .of(context)
                      .primaryColor,
                ),
                new GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (context) => new Login()));
                  },
                  child: new Text(
                    "Đăng nhập",
                    style: new TextStyle(
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),


              ],
            ),
          ),
        ],
      ),
    );
  }

  valueChange(e) {
    setState(() {
      if (e == "Nam") {
        groupValue = e;
        gender = e;
      } else if (e == "Nữ") {
        groupValue = e;
        gender = e;
      }
    });
  }

  verifyLoggin() async {
    if (fullname.text == "") {
      showSnackbar("Vui lòng nhập họ và tên !", scaffoldKey);
      return;
    }

    if (email.text == "") {
      showSnackbar("Vui lòng nhập Email !", scaffoldKey);
      return;
    }

    if (password.text == "") {
      showSnackbar("Vui lòng nhập mật khẩu !", scaffoldKey);
      return;
    }

    if (re_passwprd.text == "") {
      showSnackbar("Vui lòng nhập lại mật khẩu !", scaffoldKey);
      return;
    }

    if (location.text == "") {
      showSnackbar("Vui lòng nhập địa chỉ !", scaffoldKey);
      return;
    }

    if (phone.text == "") {
      showSnackbar("Vui lòng nhập điện thoại di động !", scaffoldKey);
      return;
    }

    if (password.text != re_passwprd.text) {
      showSnackbar("Mật khẩu nhập lại không đúng !", scaffoldKey);
      return;
    }

    displayProgressDialog(context);
    String response = await appMethods.createdUserAccount(
      fullname: fullname.text,
      password: password.text,
      groupValue: groupValue.toString(),
      email: email.text,
      location: location.text,
      phone: phone.text,
    );

    if (response ==successful) {
      closeProgressDialog(context);
      _showDialog();
    } else {
      showSnackbar(response, scaffoldKey);
      closeProgressDialog(context);
    }
  }

  void _showDialog() {
    // flutter defined function
    new AlertDialog(
      title: new Text("THÔNG BÁO"),
      content: new Text("Đăng kỳ tài khoản thành công !"),
      actions: <Widget>[
        new MaterialButton(
          onPressed: (){
            Navigator.of(context).pop(context);
          },
          child: new Text("ĐÓNG"),
        )
      ],
    );
  }
}

//Old case
//import 'package:avatar_glow/avatar_glow.dart';
//import 'package:firebase_auth/firebase_auth.dart';
//import 'package:flutter/material.dart';
//
//class SignUp extends StatefulWidget {
//  @override
//  _SignUpState createState() => _SignUpState();
//}
//
//class _SignUpState extends State<SignUp> {
//  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
//  final _formKey = GlobalKey<FormState>();
//  TextEditingController _emailTextController = TextEditingController();
//  TextEditingController _passwordTextController = TextEditingController();
//  TextEditingController _nameTextController = TextEditingController();
//  TextEditingController _confirmPasswordTextController =
//      TextEditingController();
//  TextEditingController _phoneTextController = TextEditingController();
//  TextEditingController _addressTextController = TextEditingController();
//  String gender;
//  String groupValue = "Nam";
//
//  bool loading = false;
//  bool hidePass = true;
//
//  @override
//  Widget build(BuildContext context) {
//    double height = MediaQuery.of(context).size.height / 3;
//    return Scaffold(
//      body: Stack(
//        children: <Widget>[
//          Container(
//            decoration: BoxDecoration(
//                image: new DecorationImage(
//              image: new ExactAssetImage('images/login-bg.jpg'),
//              fit: BoxFit.cover,
//            )),
//          ),
//////TODO:: make the logo show?
//          Container(
//            color: Colors.black.withOpacity(0.7),
//            width: double.infinity,
//            height: double.infinity,
//          ),
//
//          Padding(
//            padding: const EdgeInsets.only(top: 40.0),
//            child: Center(
//              child: Form(
//                key: _formKey,
//                child: Column(
//                  children: <Widget>[
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
//                      child: Material(
//                        borderRadius: BorderRadius.circular(10.0),
//                        color: Colors.white.withOpacity(0.8),
//                        elevation: 0.0,
//                        child: Padding(
//                          padding: const EdgeInsets.only(left: 12.0),
//                          child: TextFormField(
//                            controller: _nameTextController,
//                            decoration: InputDecoration(
//                              hintText: "Họ và tên",
//                              icon: Icon(Icons.person),
//                              border: InputBorder.none,
//                            ),
//                            validator: (value) {
//                              if (value.isEmpty) {
//                                return "Vui lòng không để trống họ và tên!";
//                              }
//                              return null;
//                            },
//                          ),
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
//                      child: new Container(
//                        color: Colors.white.withOpacity(0.4),
//                        child: Row(
//                          children: <Widget>[
//                            Expanded(
//                                child: ListTile(
//                              title: Text("Nam",
//                                  textAlign: TextAlign.end,
//                                  style: TextStyle(color: Colors.white)),
//                              trailing: Radio(
//                                activeColor: Colors.lightBlue,
//                                value: "Nam",
//                                groupValue: groupValue,
//                                onChanged: (e) => valueChange(e),
//                              ),
//                            )),
//                            Expanded(
//                                child: ListTile(
//                              title: Text("Nữ",
//                                  textAlign: TextAlign.end,
//                                  style: TextStyle(color: Colors.white)),
//                              trailing: Radio(
//                                activeColor: Colors.lightBlue,
//                                value: "Nữ",
//                                groupValue: groupValue,
//                                onChanged: (e) => valueChange(e),
//                              ),
//                            )),
//                          ],
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
//                      child: Material(
//                        borderRadius: BorderRadius.circular(10.0),
//                        color: Colors.white.withOpacity(0.8),
//                        elevation: 0.0,
//                        child: Padding(
//                          padding: const EdgeInsets.only(left: 12.0),
//                          child: TextFormField(
//                            controller: _emailTextController,
//                            decoration: InputDecoration(
//                              hintText: "Email",
//                              icon: Icon(Icons.email),
//                              border: InputBorder.none,
//                            ),
//                            validator: (value) {
//                              if (value.isEmpty) {
//                                Pattern pattern =
//                                    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
//                                RegExp regex = new RegExp(pattern);
//                                if (!regex.hasMatch(value))
//                                  return "Đia chỉ Email không hợp lệ!";
//                                else
//                                  return null;
//                              }
//                              return null;
//                            },
//                          ),
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
//                      child: Material(
//                        borderRadius: BorderRadius.circular(10.0),
//                        color: Colors.white.withOpacity(0.8),
//                        elevation: 0.0,
//                        child: Padding(
//                          padding: const EdgeInsets.only(left: 12.0),
//                          child: ListTile(
//                            title: TextFormField(
//                              controller: _passwordTextController,
//                              obscureText: hidePass,
//                              decoration: InputDecoration(
//                                hintText: "Mật khẩu",
//                                icon: Icon(Icons.lock_outline),
//                                border: InputBorder.none,
//                              ),
//                              validator: (value) {
//                                if (value.isEmpty) {
//                                  return "Vui lòng không để trống mật khẩu!";
//                                } else if (value.length < 6) {
//                                  return "Mật khẩu phải từ 6 ký tự trở lên";
//                                }
//                                return null;
//                              },
//                            ),
//                            trailing: IconButton(
//                              icon: Icon(Icons.remove_red_eye),
//                              onPressed: () {
//                                setState(() {
//                                  hidePass = false;
//                                });
//                              },
//
//                            ),
//                          ),
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
//                      child: Material(
//                        borderRadius: BorderRadius.circular(10.0),
//                        color: Colors.white.withOpacity(0.8),
//                        elevation: 0.0,
//                        child: Padding(
//                          padding: const EdgeInsets.only(left: 12.0),
//                          child: ListTile(
//                            title: TextFormField(
//                              controller: _confirmPasswordTextController,
//                              obscureText: hidePass,
//                              decoration: InputDecoration(
//                                hintText: "Nhập lại mật khẩu",
//                                icon: Icon(Icons.lock_outline),
//                                border: InputBorder.none,
//                              ),
//                              validator: (value) {
//                                if (value.isEmpty) {
//                                  return "Vui lòng không để trống mật khẩu!";
//                                } else if (value.length < 6) {
//                                  return "Mật khẩu phải từ 6 ký tự trở lên";
//                                } else if (_passwordTextController.text !=
//                                    value) {
//                                  return "Mật khẩu nhập lại không đúng!";
//                                }
//                                return null;
//                              },
//                            ),
//                            trailing: IconButton(
//                              icon: Icon(Icons.remove_red_eye),
//                              onPressed: () {
//                                setState(() {
//                                  hidePass = false;
//                                });
//                              },
//                            ),                          ),
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
//                      child: Material(
//                        borderRadius: BorderRadius.circular(10.0),
//                        color: Colors.white.withOpacity(0.8),
//                        elevation: 0.0,
//                        child: Padding(
//                          padding: const EdgeInsets.only(left: 12.0),
//                          child: TextFormField(
//                            controller: _phoneTextController,
//                            decoration: InputDecoration(
//                              hintText: "Số điện thoại",
//                              icon: Icon(Icons.contact_phone),
//                              border: InputBorder.none,
//                            ),
//                            validator: (value) {
//                              if (value.isEmpty) {
//                                Pattern pattern = r'^(?:[+0]9)?[0-9]{10}$';
//                                RegExp regex = new RegExp(pattern);
//                                if (!regex.hasMatch(value))
//                                  return "Số điện thoại không hợp lệ!";
//                                else
//                                  return null;
//                              }
//                              return null;
//                            },
//                          ),
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
//                      child: Material(
//                        borderRadius: BorderRadius.circular(10.0),
//                        color: Colors.white.withOpacity(0.8),
//                        elevation: 0.0,
//                        child: Padding(
//                          padding: const EdgeInsets.only(left: 12.0),
//                          child: TextFormField(
//                            controller: _addressTextController,
//                            decoration: InputDecoration(
//                              hintText: "Địa chỉ",
//                              icon: Icon(Icons.person_pin_circle),
//                              border: InputBorder.none,
//                            ),
//                            validator: (value) {
//                              if (value.isEmpty) {
//                                return "Vui lòng không để trống địa chỉ!";
//                              }
//                              return null;
//                            },
//                          ),
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.fromLTRB(14.0, 8.0, 14.0, 8.0),
//                      child: Material(
//                        borderRadius: BorderRadius.circular(20.0),
//                        color: Colors.lightBlue.withOpacity(0.8),
//                        elevation: 0.0,
//                        child: MaterialButton(
//                          onPressed: () {},
//                          minWidth: MediaQuery.of(context).size.width,
//                          child: Text(
//                            "Đăng ký",
//                            textAlign: TextAlign.center,
//                            style: TextStyle(
//                                color: Colors.white,
//                                fontWeight: FontWeight.bold,
//                                fontSize: 22.0),
//                          ),
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.all(8.0),
//                      child: InkWell(
//                        onTap: () {
//                          Navigator.pop(context);
//                        },
//                        child: Text("Đăng nhập!",
//                            textAlign: TextAlign.center,
//                            style: TextStyle(color: Colors.red)),
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//            ),
//          ),
//
//          Visibility(
//            visible: loading ?? true,
//            child: Center(
//              child: Container(
//                alignment: Alignment.center,
//                color: Colors.white.withOpacity(0.9),
//                child: CircularProgressIndicator(
//                  valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
//                ),
//              ),
//            ),
//          ),
//        ],
//      ),
//    );
//  }
//
//  valueChange(e) {
//    setState(() {
//      if (e == "Nam") {
//        groupValue = e;
//        gender = e;
//      } else if (e == "Nữ") {
//        groupValue = e;
//        gender = e;
//      }
//    });
//  }
//}
