import 'package:flutter/material.dart';
import 'package:flutter_shopapp/components/product_grid.dart';
import 'package:flutter_shopapp/components/products.dart';
import 'package:flutter_shopapp/pages/cart.dart';
import 'package:flutter_shopapp/pages/home.dart';

import 'cart.dart';

class SalePage extends StatefulWidget {
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<SalePage> {
  ScrollController _controller;

  @override
  void initState() {
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
  }

  _scrollListener() {
    if (_controller.offset > _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }

    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          elevation: 0.1, //đổ bóng phía dưới thanh AppBar
          backgroundColor: Colors.red,
          leading: new IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new HomePage()));
              }),
          title: Material(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.grey[100],
            elevation: 0.0,
            child: TextFormField(
              //controller: ,
              decoration: InputDecoration(
                  contentPadding: const EdgeInsets.all(8.0),
                  icon: Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Icon(
                      Icons.search,
                      color: Colors.red,
                    ),
                  ),
                  hintText: "Tìm kiếm...",
                  border: InputBorder.none),
              validator: (value) {
                if (value.isEmpty) {
                  return "Vui lòng nhập nội dung cần tìm";
                }
                return null;
              },
            ),
          ),
          actions: <Widget>[
            new IconButton(
                icon: Icon(
                  Icons.shopping_cart,
                  color: Colors.white,
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => new CartPage()));
                })
          ],
        ),
        body: new Center(
          child: new Container(
            child: NestedScrollView(
              controller: _controller,
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return [
                  SliverAppBar(
                    automaticallyImplyLeading: false,
                    elevation: 8,
                    pinned: true,
                    expandedHeight: 220.0,
                    title: Center(
                      child: Text(
                        "Săn SALE giá cực sốc",
                        style: TextStyle(color: Colors.red,fontFamily: 'Lobster',fontSize: 30,fontWeight: FontWeight.bold),
                      ),
                    ),
                    titleSpacing: 0.0,
                    flexibleSpace: FlexibleSpaceBar(
                      titlePadding: EdgeInsets.all(0.0),
                      background: Image.asset(
                        'images/sale-sliver-bg.jpg',
                        fit: BoxFit.fill,
                      ),
                    ),
                    backgroundColor: Colors.white,
                  ),
                ];
              },
              body: Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ProductGridView(),
                ),
              ),
            ),
          ),
        ));
  }
}
