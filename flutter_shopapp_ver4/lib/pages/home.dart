import 'dart:convert';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_shopapp/components/app_data.dart';
import 'package:flutter_shopapp/components/app_methods.dart';
import 'package:flutter_shopapp/components/app_tools.dart';
import 'package:flutter_shopapp/components/firebase_methods.dart';

//My own imports
import 'package:flutter_shopapp/components/horizontal_listview.dart';
import 'package:flutter_shopapp/components/category_horizontal_listview.dart';
import 'package:flutter_shopapp/components/products.dart';
import 'package:flutter_shopapp/pages/cart.dart';
import 'cart.dart';
import 'package:flutter_shopapp/pages/sale.dart';
import 'category.dart';
import 'login.dart';
import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  BuildContext context;
  String accName = "";
  String accEmail = "";
  String accPhotoURL = "";
  bool isLoggedIn;
  AppMethods appMethods = new FirebaseMethods();
  ScrollController _controller;
  DateTime dayNow;

  @override
  void initState() {
    dayNow = DateTime.now();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    super.initState();
    getCurrentUser();
  }

  _scrollListener() {
    if (_controller.offset > _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }

    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {});
    }
  }

  Future getCurrentUser() async {
    accName = getStringDataLocally(key: accFullName);
    accEmail = getStringDataLocally(key: emailAddress);
    accPhotoURL = getStringDataLocally(key: photoURL);
    isLoggedIn = getBoolDataLocally(key: loggedIN);
    print(await getStringDataLocally(key: emailAddress));
    accName == null ? accName = "Test Name" : accName;
    accEmail == null ? accEmail = "test@gmail.com" : accEmail;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    Widget image_carousel = new Container(
      padding: const EdgeInsets.all(15.0),
      height: 200.0,
      child: StreamBuilder(
          stream: Firestore.instance.collection('Carousel').snapshots(),
          builder: (context, snapshot) {
            if (snapshot.hasError) return new Text('Lỗi: ${snapshot.error}');
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return new Center(
                  child: Text('Đang tải...'),
                );
              default:
                return new ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: new Carousel(
                    boxFit: BoxFit.cover,
                    images: [
                      NetworkImage(snapshot.data.documents[0]['url']),
                      NetworkImage(snapshot.data.documents[1]['url']),
                      NetworkImage(snapshot.data.documents[2]['url']),
                      NetworkImage(snapshot.data.documents[4]['url']),
                      NetworkImage(snapshot.data.documents[3]['url']),
                    ],
                    autoplay: true,
                    autoplayDuration: Duration(milliseconds: 6000),
                    animationCurve: Curves.fastOutSlowIn,
                    animationDuration: Duration(milliseconds: 1000),
                    dotSize: 4.0,
                    dotColor: Colors.white,
                    dotBgColor: Colors.transparent,
                    indicatorBgPadding:
                        4.0, //padding đến box chứa các dot/padding khung chứa các dot chuyển carousel
                  ),
                );
            }
          }),
    );

    Widget promotion_product_title = new Padding(
      padding: const EdgeInsets.only(left: 2.0, right: 2.0),
      child: Container(
        child: StreamBuilder<QuerySnapshot>(
          stream: Firestore.instance.collection("Sale").snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            return new ListTile(
                subtitle: Container(
                  child: Column(
                    children: snapshot.data.documents
                        .map((DocumentSnapshot document) {

                        if (checkDateSale(document['start'], document['end'])==true) {

                            return Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Text("Từ ngày"),
                                    Text(getDate(DateTime.parse(document['start'])),
                                        style: TextStyle(
                                            fontSize: 15.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Text("Đến ngày "),
                                    Text(getDate(DateTime.parse(document['end'])),
                                        style: TextStyle(
                                            fontSize: 15.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black)),
                                  ],
                                ),
                              ],
                            );
                          }
                        else
                          {
                            return Row();
                          }
                    }).toList(),
                  ),
                ),
                title: new Text(
                  "Khuyến mãi giá sốc",
                  style: TextStyle(
                      fontSize: 25.0,
                      fontFamily: 'Lobster',
                      fontWeight: FontWeight.bold,
                      color: Colors.red),
                ),
                trailing: new RaisedButton(
                  color: Colors.red,
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => new SalePage()));
                  },
                  child: Text('Xem ngay >',
                      style: TextStyle(fontSize: 13.0, color: Colors.white)),
                ));
          },
        ),
      ),
    );

    return Scaffold(
      appBar: new AppBar(
        elevation: 0.1, //đổ bóng phía dưới thanh AppBar
        backgroundColor: Colors.red,
        title: Material(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.grey[100],
          elevation: 0.0,
          child: TextFormField(
            //controller: ,
            decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(8.0),
                icon: Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Icon(
                    Icons.search,
                    color: Colors.red,
                  ),
                ),
                hintText: "Tìm kiếm...",
                border: InputBorder.none),
            validator: (value) {
              if (value.isEmpty) {
                return "Vui lòng nhập nội dung cần tìm";
              }
              return null;
            },
          ),
        ),
        actions: <Widget>[
          new IconButton(
              icon: Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new CartPage()));
              })
        ],
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
//            header
            new UserAccountsDrawerHeader(
              accountName: Text(
                accName,
                style: TextStyle(color: Colors.white),
              ),
              accountEmail: Text(
                accEmail,
                style: TextStyle(color: Colors.white),
              ),
              currentAccountPicture: GestureDetector(
                child: new CircleAvatar(
                  backgroundColor: Colors.grey,
                  child: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                ),
              ),
              decoration: new BoxDecoration(
                color: Colors.red,
              ),
            ),

//          body
            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text('Trang chủ'),
                leading: Icon(Icons.home, color: Colors.red),
              ),
            ),

            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text('Tài khoản'),
                leading: Icon(Icons.person, color: Colors.red),
              ),
            ),

            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text('Đơn hàng của tôi'),
                leading: Icon(Icons.shopping_basket, color: Colors.red),
              ),
            ),

            InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new CartPage()));
              },
              child: ListTile(
                title: Text('Giỏ hàng'),
                leading: Icon(
                  Icons.shopping_cart,
                  color: Colors.red,
                ),
              ),
            ),

            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text('Yêu thích'),
                leading: Icon(Icons.favorite, color: Colors.red),
              ),
            ),

            Divider(),

            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text('About'),
                leading: Icon(Icons.help),
              ),
            ),

            InkWell(
              onTap: checkIfLoggedIn,
              child: ListTile(
                title: new Text(isLoggedIn == true ? "Đăng xuất" : "Đăng nhập"),
                leading: Icon(Icons.exit_to_app),
              ),
            ),
          ],
        ),
      ),
      body: new Center(
        child: new Container(
          child: NestedScrollView(
            controller: _controller,
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return [
                SliverAppBar(
                  automaticallyImplyLeading: false,
                  elevation: 8,
                  pinned: true,
                  expandedHeight: 220.0,
                  title: Container(
                    child: StreamBuilder(
                      stream:
                          Firestore.instance.collection("Category").snapshots(),
                      builder: (context, snapshot) {
                        return ListView(
                          scrollDirection: Axis.horizontal,
                          children: horizontalCategoryList(snapshot),
                        );
                      },
                    ),
                  ),
                  titleSpacing: 0.0,
                  flexibleSpace: FlexibleSpaceBar(
                    titlePadding: EdgeInsets.all(0.0),
                    background: Image.asset(
                      'images/sliver_bar_bg.png',
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  backgroundColor: Colors.white,
                ),
              ];
            },
            body: ListView(
              children: <Widget>[
                image_carousel,
                Divider(
                  color: Colors.grey[300],
                  thickness: 5.0,
                ),

                //Title of Category
                promotion_product_title,

                //Horizontal List Products of Category
                Products(),

                ////

                Divider(
                  color: Colors.grey[300],
                  thickness: 5.0,
                ),

                //Title of Category
                Padding(
                  padding: const EdgeInsets.only(left: 2.0, right: 2.0),
                  child: Container(
                    child: ListTile(
                      title: new Text("Áo thun",
                          style: TextStyle(
                              fontSize: 20.0,
                              fontFamily: 'Lobster',
                              fontWeight: FontWeight.bold)),
                      trailing: new InkWell(
                        child: Text('Xem tất cả >',
                            style: TextStyle(
                                fontSize: 13.0, color: Colors.deepOrange)),
                        onTap: () {},
                      ),
                    ),
                  ),
                ),
                Products(),

                ////
                Divider(
                  color: Colors.grey[300],
                  thickness: 5.0,
                ),

                //Title of Category
                Padding(
                  padding: const EdgeInsets.only(left: 2.0, right: 2.0),
                  child: Container(
                    child: ListTile(
                      title: new Text("Áo thun",
                          style: TextStyle(
                              fontSize: 20.0,
                              fontFamily: 'Lobster',
                              fontWeight: FontWeight.bold)),
                      trailing: new InkWell(
                        child: Text('Xem tất cả >',
                            style: TextStyle(
                                fontSize: 13.0, color: Colors.deepOrange)),
                        onTap: () {},
                      ),
                    ),
                  ),
                ),
                Products(),

                ////
                Divider(
                  color: Colors.grey[300],
                  thickness: 5.0,
                ),

                //Title of Category
                Padding(
                  padding: const EdgeInsets.only(left: 2.0, right: 2.0),
                  child: Container(
                    child: ListTile(
                      title: new Text("Áo thun",
                          style: TextStyle(
                              fontSize: 20.0,
                              fontFamily: 'Lobster',
                              fontWeight: FontWeight.bold)),
                      trailing: new InkWell(
                        child: Text('Xem tất cả >',
                            style: TextStyle(
                                fontSize: 13.0, color: Colors.deepOrange)),
                        onTap: () {},
                      ),
                    ),
                  ),
                ),
                Products(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  checkIfLoggedIn() async {
    if (isLoggedIn == false) {
      bool response = await Navigator.of(context).push(new CupertinoPageRoute(
          builder: (BuildContext context) => new Login()));
      if (response == true) {}
      return;
    }

    bool response = await appMethods.logOutUser();
    if (response == true) {
      getCurrentUser();
    }
  }

  List<Widget> horizontalCategoryList(AsyncSnapshot snapshot) {
    return snapshot.data.documents.map<Widget>((document) {
      return Padding(
        padding: const EdgeInsets.all(2.0),
        child: InkWell(
          onTap: () {},
          child: Container(
            width: 75.0,
            child: ListTile(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => new CategoryPage()));
              },
              title: Image.network(
                document['img'],
                width: 60.0,
                height: 60.0,
              ),
            ),
          ),
        ),
      );
    }).toList();
  }

  getDate(DateTime dateTimeInput) {
    String processedDate;
    processedDate = DateFormat('dd-MM-yyyy').format(dateTimeInput);
    return processedDate;
  }

  bool checkDateSale(String str_start_date, String str_end_date) {
    if (dayNow.isAfter(DateTime.parse(str_start_date)) &&
          dayNow.isBefore(DateTime.parse(str_end_date))) {
        return true;
      } else
        return false;
  }




//  getSaleProduct(DateTime timeNow) {}
//
//  String getStartSale(AsyncSnapshot snapshot) {
//    return snapshot.data.documents.map<String>((document) {
//      if (dayNow.isAfter(DateTime.parse(document['start'])) &&
//          dayNow.isBefore(DateTime.parse(document['start']))) {
//        return Text(getDate(DateTime.parse(document['start'])),style: TextStyle(
//            fontSize: 15.0,
//            fontWeight: FontWeight.bold,
//            color: Colors.black));
//      }else
//        return Text("Chưa có khuyến mãi trong thời điểm này",style: TextStyle(
//            fontSize: 15.0,
//            fontWeight: FontWeight.bold,
//            color: Colors.black));
//    });
//  }
//
//  String getEndSale(AsyncSnapshot snapshot) {
//    return snapshot.data.documents.map<String>((document) {
//      if (dayNow.isAfter(DateTime.parse(document['start'])) &&
//          dayNow.isBefore(DateTime.parse(document['start']))) {
//        return Text(getDate(DateTime.parse(document['start'])),style: TextStyle(
//            fontSize: 15.0,
//            fontWeight: FontWeight.bold,
//            color: Colors.black));
//      }else
//        return Text("Chưa có khuyến mãi trong thời điểm này",style: TextStyle(
//            fontSize: 15.0,
//            fontWeight: FontWeight.bold,
//            color: Colors.black));
//    });
//  }

}
