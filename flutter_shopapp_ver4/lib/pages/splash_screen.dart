import 'package:flutter/material.dart';
import 'package:flutter_shopapp/pages/login.dart';
import 'package:avatar_glow/avatar_glow.dart';
import 'dart:async';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();

}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds: 3),(){
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_)=>new Login()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                image: new DecorationImage(
              image: new ExactAssetImage('images/splash-screen-background.jpg'),
              fit: BoxFit.cover,
            )),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
//              Expanded(
//                flex: 1,
//                child: Column(
//                  mainAxisAlignment: MainAxisAlignment.center,
//                  children: <Widget>[
//                    AvatarGlow(
//                      glowColor: Colors.red,
//                      endRadius: 90.0,
//                      duration: Duration(milliseconds: 2000),
//                      repeat: true,
//                      showTwoGlows: true,
//                      repeatPauseDuration: Duration(milliseconds: 100),
//                      child: Material(
//                        elevation: 8.0,
//                        shape: CircleBorder(),
//                        child: CircleAvatar(
//                          backgroundColor: Colors.grey[100],
//                          child: CircleAvatar(
//                            backgroundImage: AssetImage('images/logo.png'),
//                            radius: 60.0,
//                          ),
//                          radius: 62.0,
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.only(bottom: 60.0),
//                    ),
//
//                  ],
//                ),
//              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 600.0),
                    ),
                    CircularProgressIndicator(
                      backgroundColor: Colors.grey,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
//