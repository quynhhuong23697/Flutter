import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopapp/components/horizontal_listview.dart';
import 'package:flutter_shopapp/components/product_grid.dart';
import 'package:flutter_shopapp/main.dart';
import 'package:flutter_shopapp/pages/cart.dart';
import 'cart.dart';
import 'package:flutter_shopapp/pages/category.dart';
import './home.dart';

class ProductDetails extends StatefulWidget {
  final product_detail_name;
  final product_detail_picture;
  final product_detail_new_price;
  final product_detail_old_price;

  ProductDetails(
      {this.product_detail_name,
      this.product_detail_old_price,
      this.product_detail_picture,
      this.product_detail_new_price});
  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  String groupColor = null;
  String groupSize = null;
  int quantity = 1;

  List<DropdownMenuItem<String>> listColorDrop = [];
  List<String> colors = ["Đen","Trắng","Xám","Vàng","Đỏ","Cam","Hồng"];

  List<DropdownMenuItem<String>> listSizeDrop = [];
  List<String> sizes = ["S","M","L","XL","XXL","XXXL","Ultra nXL"];

  loadColorData() {
    listColorDrop = [];
    listColorDrop=colors.map((color)=> new DropdownMenuItem<String>(child: new Text(color),value: color,)).toList();
  }

  loadSizeData() {
    listSizeDrop = [];
    listSizeDrop=sizes.map((size)=> new DropdownMenuItem<String>(child: new Text(size),value: size,)).toList();
  }

  @override
  Widget build(BuildContext context) {
    loadColorData();
    loadSizeData();

    return Scaffold(
      appBar: new AppBar(
        leading: new IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => new HomePage()));
            }),
        elevation: 0.1, //đổ bóng phía dưới thanh AppBar
        backgroundColor: Colors.red,
        title: Text(
          'Chi tiết sản phẩm',
          style: TextStyle(
            fontSize: 25.0,
            fontFamily: 'Lobster',
          ),
        ),
        actions: <Widget>[],
      ),
      body: new ListView(
        children: <Widget>[
          new Container(
            height: 300.0,
            child: GridTile(
              child: Container(
                  color: Colors.white,
                  child: CarouselSlider(items: <Widget>[
                    Image.asset(widget.product_detail_picture),
                    Image.asset(widget.product_detail_picture),
                    Image.asset(widget.product_detail_picture),
                    Image.asset(widget.product_detail_picture),
                  ])),
              footer: new Container(
                color: Colors.white70,
                child: ListTile(
                  leading: new Text(
                    widget.product_detail_name,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0),
                  ),
                  title: new Row(
                    children: <Widget>[
                      Expanded(
                          child: new Text(
                        "\$${widget.product_detail_old_price}",
                        style: TextStyle(
                            color: Colors.grey,
                            decoration: TextDecoration.lineThrough),
                      )),
                      Expanded(
                          child: new Text(
                        "\$${widget.product_detail_new_price}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.red),
                      )),
                    ],
                  ),
                ),
              ),
            ),
          ),

          Divider(),
//--------------CHỌN MÀU SẮC & SIZE-----------------
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 15.0),
                child: DropdownButtonHideUnderline(
                  child: new DropdownButton(
                    items: listColorDrop,
                    value: groupColor,
                    iconSize: 40,
                    onChanged: (value){
                      setState(() {
                        groupColor=value;
                      });
                    },
                    hint: Text("Màu sắc",style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.red,
                        fontSize: 17)),
                  ),
                ),
              ),

              Padding(
                padding: const EdgeInsets.only(left: 30.0),
                child: DropdownButtonHideUnderline(
                  child: new DropdownButton(
                    items: listSizeDrop,
                    value: groupSize,
                    iconSize: 40,
                    onChanged: (value){
                      setState(() {
                        groupSize=value;
                      });
                    },
                    hint: Text("Size",style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.red,
                        fontSize: 17)),
                  ),
                ),
              ),
            ],
          ),
          Divider(),

////--------------CHỌN SỐ LƯỢNG-----------------
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 15.0),
                child: Column(
                  children: <Widget>[
                    Text("Số lượng",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.red,
                            fontSize: 17))
                  ],
                ),
              ),
              Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      quantity > 1
                          ? IconButton(
                              icon: new Icon(Icons.remove_circle),
                              onPressed: () => setState(() => quantity--),
                            )
                          : IconButton(
                              icon: new Icon(Icons.remove_circle),
                              onPressed: () => setState(() => quantity),
                            ),
                      new Text(quantity.toString()),
                      new IconButton(
                          icon: new Icon(Icons.add_circle),
                          onPressed: () => setState(() => quantity++))
                    ],
                  ),
                ],
              ),
            ],
          ),
          Divider(),

//==============BUTTON [BUY NOW] + [GIỎ HÀNG] + [YÊU THÍCH]===============
          Row(
            children: <Widget>[
              Expanded(
                  child: MaterialButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => new CartPage()));
                },
                color: Colors.red,
                textColor: Colors.white,
                elevation: 0.2,
                child: new Text("Mua ngay"),
              )),
              new IconButton(
                  icon: Icon(Icons.add_shopping_cart),
                  color: Colors.red,
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => new CartPage()));
                  }),
              new IconButton(
                  icon: Icon(Icons.favorite_border),
                  color: Colors.red,
                  onPressed: () {})
            ],
          ),

//=======================THÔNG TIN CHI TIẾT CỦA SẢN PHẨM===================
          Divider(),
          new ListTile(
            title: new Text(
              "Mô tả",
              style: TextStyle(
                color: Colors.red,
                fontSize: 17,
                fontWeight: FontWeight.bold,
              ),
            ),
            subtitle: new Text(
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."),
          ),
          Divider(),

          new Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: new Text(
                  "Tên sản phẩm",
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Text(widget.product_detail_name),
              )
            ],
          ),

          new Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: new Text(
                  "Xuất xứ",
                  style: TextStyle(color: Colors.grey),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Text("Mỹ"),
              )
            ],
          ),

          Divider(),

//--------------------FEEDBACK---------------------
          new ListTile(
            title: Text(
              "Đánh giá sản phẩm",
              style: TextStyle(
                color: Colors.red,
                fontSize: 17,
                fontWeight: FontWeight.bold,
              ),
            ),
            subtitle: Container(
              height: 200,
              child: ListView(
                children: <Widget>[
                  ListTile(
                    title: Text("Quỳnh Hương"),
                    trailing: Text("25/9/2019"),
                    subtitle: Text(
                        "Áo đẹp, chất lượng, chất vải mềm mịn, màu sắc giống hình mẫu"),
                  ),
                  ListTile(
                    title: Text("Quỳnh Hương"),
                    trailing: Text("25/9/2019"),
                    subtitle: Text(
                        "Áo đẹp, chất lượng, chất vải mềm mịn, màu sắc giống hình mẫu"),
                  ),
                  ListTile(
                    title: Text("Quỳnh Hương"),
                    trailing: Text("25/9/2019"),
                    subtitle: Text(
                        "Áo đẹp, chất lượng, chất vải mềm mịn, màu sắc giống hình mẫu"),
                  ),
                  ListTile(
                    title: Text("Quỳnh Hương"),
                    trailing: Text("25/9/2019"),
                    subtitle: Text(
                        "Áo đẹp, chất lượng, chất vải mềm mịn, màu sắc giống hình mẫu"),
                  ),
                  ListTile(
                    title: Text("Quỳnh Hương"),
                    trailing: Text("25/9/2019"),
                    subtitle: Text(
                        "Áo đẹp, chất lượng, chất vải mềm mịn, màu sắc giống hình mẫu"),
                  ),
                ],
              ),
            ),
          ),

          Divider(),

//===============SECTION SẢN PHẨM TƯƠNG TỰ===========
          new ListTile(
            title: Text(
              "Sản phẩm tương tự",
              style: TextStyle(
                color: Colors.red,
                fontSize: 17,
                fontWeight: FontWeight.bold,
              ),
            ),
            subtitle: Container(
              height: 200,
              child: ProductGridView(),
            ),
          ),
        ],
      ),
    );
  }

  //Các hàm xử lý radio button
  colorChange(e) {
    setState(() {
      if (e == "Trắng") {
        groupColor = e;
      } else {
        groupColor = e;
      }
    });
  }

  sizeChange(e) {
    setState(() {
      if (e == "S") {
        groupSize = e;
      } else {
        groupSize = e;
      }
    });
  }
}
