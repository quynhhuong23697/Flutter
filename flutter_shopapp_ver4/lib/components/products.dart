import 'package:flutter/material.dart';
import 'package:flutter_shopapp/pages/product_detail.dart';

import 'description_text_widget.dart';

class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  var product_list = [
    {
      "name": "Blazer",
      "picture": "images/products/blazer1.jpeg",
      "old_price": 120,
      "price": 85,
    },
    {
      "name": "Blazer",
      "picture": "images/products/blazer2.jpeg",
      "old_price": 100,
      "price": 50,
    },
    {
      "name": "Red dress",
      "picture": "images/products/dress1.jpeg",
      "old_price": 100,
      "price": 50,
    },
    {
      "name": "Hill",
      "picture": "images/products/hills1.jpeg",
      "old_price": 100,
      "price": 50,
    },
    {
      "name": "Pants",
      "picture": "images/products/pants1.jpg",
      "old_price": 100,
      "price": 50,
    },
    {
      "name": "Shoe",
      "picture": "images/products/shoe1.jpg",
      "old_price": 100,
      "price": 50,
    },
  ];
  @override
  Widget build(BuildContext context) {
          return Container(
            padding: const EdgeInsets.only(left: 5.0),
              height: 200.0,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: product_list.length,
                  itemBuilder: (BuildContext context,int index) {
                    return FeaturedCard(
                      name: product_list[index]['name'],
                      price: product_list[index]['price'],
                      picture:product_list[index]['picture'],
                    );
                  }));
        }
  }


//class Single_prod extends StatelessWidget {
//  final pro_name;
//  final pro_picture;
//  final pro_old_price;
//  final pro_price;
//
//  Single_prod(
//      {this.pro_name, this.pro_picture, this.pro_old_price, this.pro_price});
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//        height: 200,
//        width: 200,
//        child: ListView.builder(
//            scrollDirection: Axis.horizontal,
//            itemCount: 4,
//            itemBuilder: (_, index) {
//              return FeaturedCard(
//                name: pro_name,
//                price: pro_price,
//                picture: pro_picture,
//              );
//            }));
//  }
//}

class FeaturedCard extends StatelessWidget {
  final String name;
  final int price;
  final String picture;

  FeaturedCard(
      {@required this.name, @required this.price, @required this.picture});


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(4),
      child: InkWell(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => ProductDetails(
            product_detail_name: name,
            product_detail_new_price: price,
            product_detail_picture: picture,
            product_detail_old_price: price,
          )));
        },
        child: Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey[300],
                offset: Offset(
                    10, //đổ ngang qua phải 10
                    10 //đổ dọc xuống 10
                ),
                blurRadius: 14,
              ),
            ],
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: Stack(
              children: <Widget>[
                Image.asset(
                  picture,
                  height: 220,
                  width: 200,
                  fit: BoxFit.cover,
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      height: 100,
                      width: 200,
                      decoration: BoxDecoration(
                        // Box decoration takes a gradient
                        gradient: LinearGradient(
                          // Where the linear gradient begins and ends
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter,
                          // Add one stop for each color. Stops should increase from 0 to 1
                          colors: [
                            // Colors are easy thanks to Flutter's Colors class.
                            Colors.black.withOpacity(0.8),
                            Colors.black.withOpacity(0.7),
                            Colors.black.withOpacity(0.6),
                            Colors.black.withOpacity(0.6),
                            Colors.black.withOpacity(0.4),
                            Colors.black.withOpacity(0.1),
                            Colors.black.withOpacity(0.05),
                            Colors.black.withOpacity(0.025),
                          ],
                        ),
                      ),
                      child: Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container())),
                ),
                Align(
                  alignment: Alignment.bottomLeft,
                  child: Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: RichText(
                          text: TextSpan(children: [
                        TextSpan(
                            text: '$name \n',style: TextStyle(fontSize: 17)),
                        TextSpan(
                            text: '\$${price.toString()} \n',
                            style: TextStyle(
                                fontSize: 22, fontWeight: FontWeight.bold)),
                      ]))),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

//return Card(
//child: Hero(
//tag: new Text("hero 1"),
//child: Material(
//child: InkWell(
//onTap: () => Navigator.of(context).push(new MaterialPageRoute(
//builder: (context) => new ProductDetails(
////truyền thông tin sản phẩm qua trang Product Detail
//product_detail_name: pro_name,
//product_detail_old_price: pro_old_price,
//product_detail_picture: pro_picture,
//product_detail_new_price: pro_price,
//))),
//child: GridTile(
//footer: Container(
//color: Colors.white70,
//child: new Row(
//children: <Widget>[
//Expanded(
//child: new Text(
//pro_name,
//style: TextStyle(
//fontWeight: FontWeight.bold, fontSize: 16.0),
//),
//),
//new Text(
//"\$${pro_price}",
//style: TextStyle(
//color: Colors.red, fontWeight: FontWeight.bold),
//)
//],
//),
//),
//child: Image.asset(
//pro_picture,
//fit: BoxFit.cover,
//)),
//),
//),
//),
//);
