import 'package:flutter/material.dart';
import 'package:flutter_shopapp/components/products.dart';

class CategoryHorizontalList extends StatelessWidget {


  var listCategoryName = [
    {
      "name": "Áo Thun",
      "name": "Áo khoác",
      "name": "Váy",
      "name": "Quần",
      "name": "Giày",
      "name": "Phụ kiện"
    }
  ];


  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: listCategoryName.length,
      scrollDirection: Axis.vertical,
      itemBuilder: (BuildContext context, int index) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Divider(
              color: Colors.grey[300],
              thickness: 5.0,
            ),

            Category(
              cat_name: "Áo Thun",
            ),
            //Title of Category


            //Horizontal List Products of Category
            Products(),
          ],
        );
      },
    );
  }
}

// ignore: must_be_immutable
class Category extends StatelessWidget {
  final String cat_name;

  Category({
    this.cat_name,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 2.0, right: 2.0),
      child: Container(
        child: ListTile(
          title: new Text(cat_name,
              style: TextStyle(
                  fontSize: 18.0, fontWeight: FontWeight.bold)),
          trailing: new InkWell(
            child: Text('Xem tất cả >',
                style: TextStyle(
                    fontSize: 13.0, color: Colors.deepOrange)),
            onTap: () {},
          ),
        ),
      ),
    );
  }
}






