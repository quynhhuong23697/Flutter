import 'dart:ui';

import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shopapp/pages/category.dart';

class HorizontalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
      Container(
      height: 100.0,
      //margin: EdgeInsets.symmetric(vertical: 0.0),       //TUANTLV ADD 10/5
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Category(
            image_location: 'images/cats/tshirt.png',
            image_caption: 'Áo thun',
          ),

          Category(
            image_location: 'images/cats/jacket.png',
            image_caption: 'Áo khoác',
          ),

          Category(
            image_location: 'images/cats/dress.png',
            image_caption: 'Váy',
          ),

          Category(
            image_location: 'images/cats/jeans.png',
            image_caption: 'Jeans',
          ),

          Category(
            image_location: 'images/cats/shoe.png',
            image_caption: 'Giày',
          ),

          Category(
            image_location: 'images/cats/accessories.png',
            image_caption: 'Phụ kiện',
          ),
        ],
      ),
    );
  }
}

class Category extends StatelessWidget {
  final String image_location;
  final String image_caption;

  Category({
    this.image_caption,
    this.image_location,
});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: InkWell(
        onTap: () {},
        child: Container(
          width: 75.0,
          child: ListTile(
            onTap: (){
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => new CategoryPage()));
            },
              title: Image.asset(
                image_location,
                width: 60.0,
                height: 60.0,
              ),
          ),
        ),
      ),
    );
  }
}


