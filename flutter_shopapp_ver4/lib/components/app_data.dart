



//Database
const String userData="userData";

//Database Collections
const String userCollection="User";

//User details
const String userID = "userID";
const String userGender="gender";
const String accFullName = "accFullName";
const String phoneNumber = "phone";
const String emailAddress = "email";
const String userPassword = "password";
const String userLocation="location";
const String signUpDate="signupdate";
const String photoURL="photoURL";
const String loggedIN="loggedIN";

//app_data
const String successful="successful";
