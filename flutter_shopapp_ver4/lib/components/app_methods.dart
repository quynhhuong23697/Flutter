import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

abstract class AppMethods{
Future<String> loginUser({String email, String password});
Future<String> createdUserAccount({String fullname, String groupValue, String password, String email, String location, String phone});
Future <bool> logOutUser();
Future<DocumentSnapshot> getUserInfo(String userid);
}

