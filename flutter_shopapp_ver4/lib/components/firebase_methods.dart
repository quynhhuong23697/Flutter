import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_shopapp/components/app_data.dart';
import 'package:flutter_shopapp/components/app_methods.dart';
import 'package:flutter_shopapp/components/app_tools.dart';
import 'app_tools.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'dart:async';

class FirebaseMethods implements AppMethods {
  Firestore firestore = Firestore.instance;
  final FirebaseAuth auth = FirebaseAuth.instance;

  @override
  Future<String> createdUserAccount(
      {String fullname,
      String groupValue,
      String password,
      String email,
      String location,
      String phone}) async {
    FirebaseUser user;


//    AuthResult result = await auth.createUserWithEmailAndPassword(
//        email: email, password: password);
//    user = result.user;

    try {
      AuthResult result = await auth.createUserWithEmailAndPassword(
          email: email, password: password);
      user = result.user;

      if (user != null) {
        await firestore.collection(userCollection).document(user.uid).setData({
          userID: user.uid,
          accFullName: fullname,
          emailAddress: email,
          userGender: groupValue,
          userPassword: password,
          phoneNumber: phone,
          userLocation: location,
          signUpDate: DateTime.now().toString(),
        });

        writeDataLocally(key: userID, value: user.uid);
        writeDataLocally(key: fullname, value: fullname);
        writeDataLocally(key: emailAddress, value: email);
        writeDataLocally(key: userPassword, value: password);
        writeDataLocally(key: phoneNumber, value: phone);
        writeDataLocally(key: userLocation, value: location);
        writeDataLocally(key: userGender, value: groupValue);
        writeDataLocally(key: signUpDate, value: DateTime.now().toString());
      }
    } on PlatformException catch (e) {
      return errorMSG(e.details);
    }
    return user == null ? errorMSG("Lỗi") : successfulMSG();
  }

  @override
  Future<String> loginUser({String email, String password}) async {
    FirebaseUser user;
    try {
      AuthResult result = await auth.signInWithEmailAndPassword(
          email: email, password: password);
      user = result.user;

      user=(await auth.signInWithEmailAndPassword(email: email, password: password)).user;

      if (user != null) {
        DocumentSnapshot userInfo = await getUserInfo(user.uid);
        await writeDataLocally(key: userID, value: userInfo[userID]);
        await writeDataLocally(key: accFullName, value: userInfo[accFullName]);
        await writeDataLocally(
            key: emailAddress, value: userInfo[emailAddress]);
        await writeDataLocally(key: phoneNumber, value: userInfo[phoneNumber]);
        await writeBoolDataLocally(key: loggedIN, value: true);

        print(userInfo[emailAddress]);
      }
    } on PlatformException catch (e) {
      //
      return errorMSG(e.details);
    }
    return user == null ? errorMSG("Lỗi") : successfulMSG();
  }

  Future<bool> complete() async {
    return true;
  }

  Future<bool> notComplete() async {
    return false;
  }

  Future<String> successfulMSG() async {
    return successful;
  }

  Future<String> errorMSG(String e) async {
    return e;
  }

  @override
  Future<bool> logOutUser() async {
    await auth.signOut();
    await clearDataLocally();
    return complete();
  }

  @override
  Future<DocumentSnapshot> getUserInfo(String userid) async {
    return await firestore.collection(userCollection).document(userid).get();
  }
}
