import 'package:flutter_shopapp/main.dart';
import 'package:flutter/material.dart';

class Cart_products extends StatefulWidget {
  @override
  _Cart_productsState createState() => _Cart_productsState();
}

class _Cart_productsState extends State<Cart_products> {
  var Products_on_the_cart = [
    {
      "name": "Men Blazer",
      "picture": "images/products/blazer1.jpeg",
      "price": 130,
      "size": "L",
      "color": "White",
      "quality": 2,
    },
    {
      "name": "Women Blazer",
      "picture": "images/products/blazer2.jpeg",
      "price": 120,
      "size": "M",
      "color": "Black",
      "quality": 1,
    },
    {
      "name": "Women Blazer",
      "picture": "images/products/blazer2.jpeg",
      "price": 120,
      "size": "M",
      "color": "Black",
      "quality": 1,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
        itemCount: Products_on_the_cart.length,
        itemBuilder: (context, index) {
          return Single_cart_product(
            cart_pro_name: Products_on_the_cart[index]["name"],
            cart_pro_size: Products_on_the_cart[index]["size"],
            cart_pro_qty: Products_on_the_cart[index]["quality"],
            cart_pro_price: Products_on_the_cart[index]["price"],
            cart_pro_picture: Products_on_the_cart[index]["picture"],
            cart_pro_color: Products_on_the_cart[index]["color"],
          );
        });
  }
}

class Single_cart_product extends StatefulWidget {
  final cart_pro_name;
  final cart_pro_picture;
  final cart_pro_price;
  final cart_pro_size;
  final cart_pro_color;
  final cart_pro_qty;

  Single_cart_product({
    this.cart_pro_name,
    this.cart_pro_color,
    this.cart_pro_picture,
    this.cart_pro_price,
    this.cart_pro_qty,
    this.cart_pro_size,
  });

  @override
  _Single_cart_productState createState() => _Single_cart_productState();
}

class _Single_cart_productState extends State<Single_cart_product> {

  int quantity = 1;
  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(

//=================LEADING SECTION+++++++++++++++++++++++++
        leading: new Image.asset(
          widget.cart_pro_picture,
          width: 80.0,
          height: 80.0,
        ),

//=================TITLE SECTION==========================
        title: new Text(widget.cart_pro_name),

//=================SUBTITLE SECTION======================
        subtitle: new Column(
          children: <Widget>[
// Dòng 1 ở bên trong cột
            new Row(
              children: <Widget>[
// SIZE OF PRODUCT
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: new Text("Size:"),
                ),
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: new Text(
                    widget.cart_pro_size,
                    style: TextStyle(color: Colors.red),
                  ),
                ),
// COLOR OF PRODUCT
                new Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 8.0, 8.0, 8.0),
                  child: new Text("Màu:"),
                ),
                Padding(
                  padding: const EdgeInsets.all(6.0),
                  child: new Text(
                    widget.cart_pro_color,
                    style: TextStyle(color: Colors.red),
                  ),
                ),
              ],
            ),
// PRODUCT PRICE
            new Container(
              alignment: Alignment.topLeft,
              child: new Text(
                "\$${widget.cart_pro_price}",
                style: TextStyle(
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.red),
              ),
            )
          ],
        ),

        trailing: new Container(
          width: 100,
          height: 60,
          child: Row(
            children: <Widget>[
              quantity < 1
                  ? Expanded(
                child: IconButton(
                  icon: new Icon(Icons.remove_circle),
                  onPressed: (){quantity--;},
                  color: Colors.red,
                ),
              )
                  : Expanded(
                child: IconButton(
                  icon: new Icon(Icons.remove_circle),
                  onPressed: (){quantity=1;},
                  color: Colors.red,
                ),
              ),
              new Text(quantity.toString()),
              Expanded(
                child: IconButton(
                  icon: new Icon(Icons.add_circle),
                  onPressed: (){quantity++;},
                  color: Colors.red,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }


}


//class Single_cart_product1 extends StatelessWidget {
//  final cart_pro_name;
//  final cart_pro_picture;
//  final cart_pro_price;
//  final cart_pro_size;
//  final cart_pro_color;
//  final cart_pro_qty;
//int quantity=1;
//
//  Single_cart_product1({
//    this.cart_pro_name,
//    this.cart_pro_color,
//    this.cart_pro_picture,
//    this.cart_pro_price,
//    this.cart_pro_qty,
//    this.cart_pro_size,
//  });
//
//  @override
//  Widget build(BuildContext context) {
//    return Card(
//      child: ListTile(
//
////=================LEADING SECTION+++++++++++++++++++++++++
//        leading: new Image.asset(
//          cart_pro_picture,
//          width: 80.0,
//          height: 80.0,
//        ),
//
////=================TITLE SECTION==========================
//        title: new Text(cart_pro_name),
//
////=================SUBTITLE SECTION======================
//        subtitle: new Column(
//          children: <Widget>[
//// Dòng 1 ở bên trong cột
//            new Row(
//              children: <Widget>[
//// SIZE OF PRODUCT
//                Padding(
//                  padding: const EdgeInsets.all(0.0),
//                  child: new Text("Size:"),
//                ),
//                Padding(
//                  padding: const EdgeInsets.all(6.0),
//                  child: new Text(
//                    cart_pro_size,
//                    style: TextStyle(color: Colors.red),
//                  ),
//                ),
//// COLOR OF PRODUCT
//                new Padding(
//                  padding: const EdgeInsets.fromLTRB(20.0, 8.0, 8.0, 8.0),
//                  child: new Text("Màu:"),
//                ),
//                Padding(
//                  padding: const EdgeInsets.all(6.0),
//                  child: new Text(
//                    cart_pro_color,
//                    style: TextStyle(color: Colors.red),
//                  ),
//                ),
//              ],
//            ),
//// PRODUCT PRICE
//            new Container(
//              alignment: Alignment.topLeft,
//              child: new Text(
//                "\$${cart_pro_price}",
//                style: TextStyle(
//                    fontSize: 17.0,
//                    fontWeight: FontWeight.bold,
//                    color: Colors.red),
//              ),
//            )
//          ],
//        ),
//
//        trailing: new Container(
//          width: 100,
//          height: 60,
//          child: Row(
//            children: <Widget>[
//              quantity < 1
//                  ? Expanded(
//                child: IconButton(
//                  icon: new Icon(Icons.remove_circle),
//                  onPressed: (){quantity--;},
//                  color: Colors.red,
//                ),
//              )
//                  : Expanded(
//                child: IconButton(
//                  icon: new Icon(Icons.remove_circle),
//                  onPressed: (){quantity=1;},
//                  color: Colors.red,
//                ),
//              ),
//              new Text(quantity.toString()),
//              Expanded(
//                child: IconButton(
//                  icon: new Icon(Icons.add_circle),
//                  onPressed: (){quantity++;},
//                  color: Colors.red,
//                ),
//              ),
//            ],
//          ),
//        ),
//      ),
//    );
//  }
//}
//
